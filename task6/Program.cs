﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task6
{
    class Program
    {
        static void Main(string[] args)
        {
            double weight = 0, height = 0;
            
            
           Console.WriteLine("Enter the weigth in kg");
           double.TryParse(Console.ReadLine(), out weight);

          Console.WriteLine("Enter the Heigth(cm)");
          double.TryParse(Console.ReadLine(), out height);
                        
            Console.WriteLine(" Your are: " + Category(BMI(weight,height)));
            Console.ReadLine();
        }

        static double BMI(double weight,double height)
        {
            return weight / Math.Pow((height / 100), 2);
        }

        static string Category(double bmi)
        {
            string category = "";
            if (bmi < 18.5)
            {
               return category = "Underweight";
            }
            else if (bmi > 18 && bmi < 24.9)
            {
               return category = "Normal weight";
            }
            else if (bmi >= 25 && bmi <= 29.9)
            {
                return category = "Overweight";
            }
            else
            {
              return  category = "Obese";
            }
        }

    }
}
